# Polymer Flip Card

This module provides Drupal integration for Polymer `<flip-card>` element. You enable the Feature and it provides a new `<flip-card>` field that you can use anywhere that a Multifield can be used.

## Installation

1. Enable Feature, allow all dependencies to be enabled. As these modules are still in development, it would be very helpful if you can report problems in the respective queues

* `<flip-card>` — CSS issues, and feature requests for the card itself: [GitHub repo](https://github.com/rupl/flip-card).
* Issues with the incorrect display of content within the `<flip-card>`. [Use d.o/polymer_flipcard].
* Issues with Polymer Platform failing to load: [use d.o/polymer](https://www.drupal.org/project/polymer)

This Feature depends on many other Drupal modules to store configuration from many sources and provide you a drop-in field that is quite complex.

For more information about the element itself, visit the GitHub page:

https://github.com/rupl/flip-card

## Maintainers

Chris Ruppel - https://www.drupal.org/u/rupl

Made with ❤ at Four Kitchens
https://fourkitchens.com
