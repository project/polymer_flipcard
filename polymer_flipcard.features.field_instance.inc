<?php
/**
 * @file
 * polymer_flipcard.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function polymer_flipcard_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'multifield-field_polymer_flipcard-field_polymer_flipcard_axis'
  $field_instances['multifield-field_polymer_flipcard-field_polymer_flipcard_axis'] = array(
    'bundle' => 'field_polymer_flipcard',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Determines how the card flips. Defaults to Y axis, which is "left to right" flipping.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 2,
      ),
    ),
    'entity_type' => 'multifield',
    'field_name' => 'field_polymer_flipcard_axis',
    'label' => 'Rotation axis',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => 3,
    ),
  );

  // Exported field_instance: 'multifield-field_polymer_flipcard-field_polymer_flipcard_back'
  $field_instances['multifield-field_polymer_flipcard-field_polymer_flipcard_back'] = array(
    'bundle' => 'field_polymer_flipcard',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'The user can only read this content when the card has been flipped over.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 1,
      ),
    ),
    'entity_type' => 'multifield',
    'field_name' => 'field_polymer_flipcard_back',
    'label' => 'Back',
    'required' => 1,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 5,
      ),
      'type' => 'text_textarea',
      'weight' => 2,
    ),
  );

  // Exported field_instance: 'multifield-field_polymer_flipcard-field_polymer_flipcard_front'
  $field_instances['multifield-field_polymer_flipcard-field_polymer_flipcard_front'] = array(
    'bundle' => 'field_polymer_flipcard',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'This text is shown by default when the page loads.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'multifield',
    'field_name' => 'field_polymer_flipcard_front',
    'label' => 'Front',
    'required' => 1,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 5,
      ),
      'type' => 'text_textarea',
      'weight' => 1,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Back');
  t('Determines how the card flips. Defaults to Y axis, which is "left to right" flipping.');
  t('Front');
  t('Rotation axis');
  t('The user can only read this content when the card has been flipped over.');
  t('This text is shown by default when the page loads.');

  return $field_instances;
}
