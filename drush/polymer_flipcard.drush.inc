<?php
/**
 * @file
 *   Drush commands for Polymer <flip-card>.
 */

/**
 * Implements drush_MODULE_post_COMMAND().
 */
function drush_polymer_flipcard_post_pm_enable() {
  $extensions = func_get_args();

  // Deal with comma delimited extension list.
  if (strpos($extensions[0], ',') !== FALSE) {
    $extensions = explode(',', $extensions[0]);
  }

  // Download the Polymer <flip-card> library.
  if (in_array('polymer_flipcard', $extensions) && !drush_get_option('skip')) {
    _polymer_drush_download_platform();
    _polymer_drush_download('polymer_flipcard');
  }
}
