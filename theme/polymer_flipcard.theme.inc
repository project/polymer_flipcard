<?php
/**
 * @file
 *   Theme-related functions for Polymer <flip-card>.
 */

/**
 * Implements hook_theme().
 *
 * Sets template suggestions to allow themes to override output.
 */
function polymer_flipcard_theme() {
  $registry['polymer_flipcard'] = array(
    'render element' => 'elements',
    'template' => 'field--polymer-flipcard',
    'path' => drupal_get_path('module', 'polymer_flipcard') . '/theme',
  );

  return $registry;
}

/**
 * Preprocess for our custom theme function: polymer_flipcard
 *
 * Outputs a single <flip-card>.
 */
function template_preprocess_polymer_flipcard(&$vars) {
  // No preprocessing by default
}

/**
 * Implements hook_field_formatter_info().
 */
function polymer_flipcard_field_formatter_info() {
  return array(
    'polymer_flipcard' => array(
      'label' => t('<flip-card>'),
      'field types' => array('multifield'),
      'settings'  => array(),
    ),
  );
}

/**
 *
 * Implements hook_field_formatter_view().
 */
function polymer_flipcard_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $elements = array();

  foreach ($items as $delta => $item) {
    // Assemble values to be piped into markup.
    $elements[$delta] = array(
      '#markup' => theme('polymer_flipcard', array(
        'element' => $item,
        'field' => $instance,
        'axis' => filter_xss($item['field_polymer_flipcard_axis'][$langcode][0]['value']),
        'front' => filter_xss($item['field_polymer_flipcard_front'][$langcode][0]['value']),
        'back' => filter_xss($item['field_polymer_flipcard_back'][$langcode][0]['value']),
      )),
    );

    // Add Polymer <flip-card> assets.
    drupal_add_html_head(
      array(
        '#tag' => 'link',
        '#attributes' => array(
          'href' => 'sites/all/libraries/polymer_flipcard/flip-card.html',
          'rel' => 'import',
        ),
      ),
      'polymer_flipcard'
    );
  }

  return $elements;
}
