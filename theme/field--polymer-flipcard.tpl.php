<?php
/**
 * @file
 *   Default template for Polymer <flip-card>.
 *
 * @see polymer_flipcard_theme()
 *
 * This markup requires two JS libraries in order to function properly:
 * - Polymer Platform
 * - flip-card
 *
 * @see polymer_libraries_info()
 * @see polymer_flipcard_libraries_info()
 *
 * Available variables:
 * - $front: Contains the content that was entered for front side of card. This
 *   content is visible by default.
 * - $back: Contains the content entered for the back side. User must interact
 *   to discover this information.
 * - $axis: Determines how the card flips over. Two options: x for up/down
 *   rotation, y for left/right rotation. Defaults to y axis.
 *
 * Additional variables:
 * - $field: Contains standard Drupal metadata about this field instance.
 *
 * @ingroup themeable
 */
?>
<flip-card axis="<?php print $axis; ?>">
  <front><?php print $front; ?></front>
  <back><?php print $back; ?></back>
</flip-card>
