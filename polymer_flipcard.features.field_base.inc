<?php
/**
 * @file
 * polymer_flipcard.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function polymer_flipcard_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_polymer_flipcard'
  $field_bases['field_polymer_flipcard'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_polymer_flipcard',
    'foreign keys' => array(
      'field_polymer_flipcard_back_format' => array(
        'columns' => array(
          'field_polymer_flipcard_back_format' => 'format',
        ),
        'table' => 'filter_format',
      ),
      'field_polymer_flipcard_front_format' => array(
        'columns' => array(
          'field_polymer_flipcard_front_format' => 'format',
        ),
        'table' => 'filter_format',
      ),
    ),
    'indexes' => array(
      'field_polymer_flipcard_axis_value' => array(
        0 => 'field_polymer_flipcard_axis_value',
      ),
      'field_polymer_flipcard_back_format' => array(
        0 => 'field_polymer_flipcard_back_format',
      ),
      'field_polymer_flipcard_front_format' => array(
        0 => 'field_polymer_flipcard_front_format',
      ),
      'id' => array(
        0 => 'id',
      ),
    ),
    'locked' => 0,
    'module' => 'multifield',
    'settings' => array(
      'entity_translation_sync' => array(
        0 => 'id',
      ),
      'hide_blank_items' => 1,
    ),
    'translatable' => 0,
    'type' => 'multifield',
  );

  // Exported field_base: 'field_polymer_flipcard_axis'
  $field_bases['field_polymer_flipcard_axis'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_polymer_flipcard_axis',
    'foreign keys' => array(),
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'y' => 'Y axis (left-right)',
        'x' => 'X axis (up-down)',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  // Exported field_base: 'field_polymer_flipcard_back'
  $field_bases['field_polymer_flipcard_back'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_polymer_flipcard_back',
    'foreign keys' => array(
      'format' => array(
        'columns' => array(
          'format' => 'format',
        ),
        'table' => 'filter_format',
      ),
    ),
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(),
    'translatable' => 0,
    'type' => 'text_long',
  );

  // Exported field_base: 'field_polymer_flipcard_front'
  $field_bases['field_polymer_flipcard_front'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_polymer_flipcard_front',
    'foreign keys' => array(
      'format' => array(
        'columns' => array(
          'format' => 'format',
        ),
        'table' => 'filter_format',
      ),
    ),
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(),
    'translatable' => 0,
    'type' => 'text_long',
  );

  return $field_bases;
}
